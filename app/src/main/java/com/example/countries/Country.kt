package com.example.countries

data class Country(
    val name: String,
    val capital: String,
    val population: Long,
    val area: Long,
    val languages: List<Language>,
    val flag: String,
    val currencies: List<Currency>
)

data class Language(
   val name: String
)

data class Currency(
    val name: String,
    val code: String
)
