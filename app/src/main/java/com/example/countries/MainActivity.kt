package com.example.countries

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.example.countries.databinding.ActivityMainBinding
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.searchButton.setOnClickListener {
                val countryName = binding.countryNameEditText.text.toString()

                lifecycleScope.launch {
                    try {
                        val countries = restCountriesApi.getCountryByName(countryName)
                        val country = countries[0]

                        binding.countryNameTextView.text = country.name
                        binding.capitalTextView.text = country.capital
                        binding.populationTextView.text = numberDivision(country.population)
                        binding.areaTextView.text = numberDivision(country.area)
                        binding.languagesTextView.text = languagesToString(country.languages)
                        binding.currenciesTextView.text = currenciesToString(country.currencies)

                        //binding.imageView.setImageResource(R.drawable.ic_launcher_background)
                        //loadSvg(binding.imageView, country.flag)

                        Glide.with(applicationContext).load(country.flag).into(binding.imageView)

                        binding.resultLayout.visibility = View.VISIBLE
                        binding.statusLayout.visibility = View.INVISIBLE
                    }
                        catch (e: Exception){
                            binding.statusTextView.text = "Страна не найдена"
                            binding.imageView2.setImageResource(R.drawable.ic_baseline_error_outline_24)
                            binding.resultLayout.visibility = View.INVISIBLE
                            binding.statusLayout.visibility = View.VISIBLE
                        }

            }
        }
    }

}


